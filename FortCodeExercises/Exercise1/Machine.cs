namespace FortCodeExercises.Exercise1
{
    public class Machine
    {
        public string machineName = "";
        public int type = 0;
        
        public string name
        {
            get
            {
                var machineName = "";
                if (this.machineName == null || this.machineName == "")
                {
                    if (this.type == 2) machineName = "tractor";
                    else if (this.type == 0) machineName = "bulldozer";
                    else if (this.type == 1) machineName = "crane";
                    else if (this.type == 4) machineName = "car";
                    else if (this.type == 3) machineName = "truck";
                }
                return machineName;
            }
        }

        public string description
        {
            get
            {
                var hasMaxSpeed = true;
                if (this.type == 3 || this.type == 4) hasMaxSpeed = false;
                else if (this.type == 1 || this.type ==2) hasMaxSpeed = true;
                var description= " "+ this.color + " "+ this.name+" "+ "["+ this.getMaxSpeed(this.type, hasMaxSpeed) + "].";
                return description;
            }
        }

        public string color
        {
            get
            {
                var color = "white";
                if (this.type == 1) color = "blue";
                else if (this.type == 0) color = "red";
                else if (this.type == 4) color = "brown";
                else if (this.type == 3) color = "yellow";
                else if (this.type == 2) color = "green";
                return color;
            }
        }

        public string trimColor
        {
            get
            {
                var baseColor = "white";
                if (this.type == 0) baseColor = "red";
                else if (this.type == 1) baseColor = "blue";
                else if (this.type == 2) baseColor = "green";
                else if (this.type == 3) baseColor = "yellow";
                else if (this.type == 4) baseColor = "brown";
                

                var trimColor = "";
                if (this.type == 1 && this.isDark(baseColor)) trimColor = "black";
                else if (this.type == 1 && !this.isDark(baseColor)) trimColor = "white";
                else if (this.type == 2 && this.isDark(baseColor)) trimColor = "gold";
                else if (this.type == 3 && this.isDark(baseColor)) trimColor = "silver";
                return trimColor;
            }
        }

        public bool isDark(string color)
        {
            var isDark = false;
            if (color == "red" || color == "green" || color == "black" || color == "crimson") isDark = true;
            return isDark;
        }
        
        public int getMaxSpeed(int machineType, bool noMax = false) {
            var max = 70;
            if (noMax == false && machineType == 2) max = 60;
            else if (machineType == 0 && noMax == true) max = 80;
            else if (machineType == 2 && noMax == true) max = 90;
            else if (machineType == 4 && noMax == true) max = 90;
            else if (machineType == 1 && noMax == true) max = 75;
            return max;
        }
    }
}